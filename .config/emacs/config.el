;;
;;(require 'package)
;;(add-to-list 'package-archives
;;     '("melpa" . "https://melpa.org/packages/"))
;;(package-initialize)
;;(package-refresh-contents)
;;
;;  ;; This is only needed once, near the top of the file
;;(eval-when-compile
;;  ;; Following line is not needed if use-package.el is in ~/.emacs.d
;;  (add-to-list 'load-path "<path where use-package is installed>")
;;  (require 'use-package))

;; Download Evil
(unless (package-installed-p 'evil)
  (package-install 'evil))
(unless (package-installed-p 'evil-collection)
  (package-install 'evil-collection))
(unless (package-installed-p 'evil-tutor)
  (package-install 'evil-tutor))

;; Enable Evil
(setq evil-want-integration t) ;; This is optional since it's already set to t by default.
(setq evil-want-keybinding nil)
(setq evil-vsplit-window-right t)
(setq evil-split-window-below t)
(require 'evil)
(evil-mode 1)
(require 'evil-collection)
(setq evil-collection-mode-list '(dashboard dired ibuffer))
(evil-collection-init)
(require 'evil-tutor)


;; Using RETURN to follow links in Org/Evil 
;; Unmap keys in 'evil-maps if not done, (setq org-return-follows-link t) will not work
(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "SPC") nil)
  (define-key evil-motion-state-map (kbd "RET") nil)
  (define-key evil-motion-state-map (kbd "TAB") nil))
;; Setting RETURN key in org-mode to follow links
  (setq org-return-follows-link  t)

(unless (package-installed-p 'general)
  (package-install 'general))
(require 'general)

(general-evil-setup)

;; set up 'SPC' as the global leader key
(general-create-definer leaderkey
  :states '(normal insert visual emacs)
  :keymaps 'override
  :prefix "SPC" ;; set leader
  :global-prefix "M-SPC") ;; access leader in insert mode
(leaderkey
  "SPC" '(counsel-M-x :wk "Counsel M-x")
  "." '(find-file :wk "Find file")
  "f c" '((lambda () (interactive) (find-file "~/.config/emacs/config.org")) :wk "Edit emacs config")
  "f r" '(counsel-recentf :wk "Open Recent Files")
  "TAB TAB" '(comment-line :wk "Comment lines"))
(leaderkey
  "b" '(:ignore t :wk "buffer")
  "b b" '(switch-to-buffer :wk "Switch buffer")
  "b k" '(kill-current-buffer :wk "Kill this buffer")
  "b n" '(next-buffer :wk "Next buffer")
  "b p" '(previous-buffer :wk "Previous buffer")
  "b r" '(revert-buffer :wk "Reload buffer")
  "b c" '(clone-indirect-buffer :wk "Create indirect buffer copy in a split")
  "b C" '(clone-indirect-buffer-other-window :wk "Clone indirect buffer in new window")
  "b d" '(bookmark-delete :wk "Delete bookmark")
  "b i" '(ibuffer :wk "Ibuffer")
  "b K" '(kill-some-buffers :wk "Kill multiple buffers")
  "b l" '(list-bookmarks :wk "List bookmarks")
  "b m" '(bookmark-set :wk "Set bookmark")
  "b r" '(revert-buffer :wk "Reload buffer")
  "b R" '(rename-buffer :wk "Rename buffer")
  "b s" '(basic-save-buffer :wk "Save buffer")
  "b S" '(save-some-buffers :wk "Save multiple buffers")
  "b w" '(bookmark-save :wk "Save current bookmarks to bookmark file"))

(leaderkey
  "m" '(:ignore t :wk "EMMS")
  "m a" '(emms-play-m3u-playlist :wk "Open Playlist")
  "m s" '(emms-shuffle :wk "Shuffle Playlist")
  "m n" '(emms-next :wk "Next Song")
  "m p" '(emms-previous :wk "Previous Song")
  "m x" '(emms-pause :wk "Pause"))

(leaderkey
  "e" '(:ignore t :wk "Evaluate")    
  "e b" '(eval-buffer :wk "Evaluate elisp in buffer")
  "e d" '(eval-defun :wk "Evaluate defun containing or after point")
  "e e" '(eval-expression :wk "Evaluate and elisp expression")
  "e l" '(eval-last-sexp :wk "Evaluate elisp expression before point")
  "e r" '(eval-region :wk "Evaluate elisp in region")) 

 (leaderkey
  "h" '(:ignore t :wk "Help")
  "h a" '(counsel-apropos :wk "Apropos")
  "h b" '(describe-bindings :wk "Describe bindings")
  "h c" '(describe-char :wk "Describe character under cursor")
  "h d" '(:ignore t :wk "Emacs documentation")
  "h d a" '(about-emacs :wk "About Emacs")
  "h d d" '(view-emacs-debugging :wk "View Emacs debugging")
  "h d f" '(view-emacs-FAQ :wk "View Emacs FAQ")
  "h d m" '(info-emacs-manual :wk "The Emacs manual")
  "h d n" '(view-emacs-news :wk "View Emacs news")
  "h d o" '(describe-distribution :wk "How to obtain Emacs")
  "h d p" '(view-emacs-problems :wk "View Emacs problems")
  "h d t" '(view-emacs-todo :wk "View Emacs todo")
  "h d w" '(describe-no-warranty :wk "Describe no warranty")
  "h e" '(view-echo-area-messages :wk "View echo area messages")
  "h f" '(describe-function :wk "Describe function")
  "h F" '(describe-face :wk "Describe face")
  "h g" '(describe-gnu-project :wk "Describe GNU Project")
  "h i" '(info :wk "Info")
  "h I" '(describe-input-method :wk "Describe input method")
  "h k" '(describe-key :wk "Describe key")
  "h l" '(view-lossage :wk "Display recent keystrokes and the commands run")
  "h L" '(describe-language-environment :wk "Describe language environment")
  "h m" '(describe-mode :wk "Describe mode")
  "h r" '(:ignore t :wk "Reload")
  "h r r" '((lambda () (interactive) (load-file "~/.config/emacs/init.el")) :wk "Reload emacs config")
  ;;"h r r" '(reload-init-file :wk "Reload emacs config")
  "h v" '(describe-variable :wk "Describe variable"))

(leaderkey
  "d" '(:ignore t :wk "Dired")
  "d d" '(dired :wk "Open dired")
  "d n" '(neotree-dir :wk "Open directory in neotree")
  ;;"d p" '(peep-dired :wk "Peep-dired")
  "d j" '(dired-jump :wk "Dired jump to current"))

(general-define-key
 :states '(normal)
 :keymaps 'dired-mode-map
 "b" '(:ignore t :which-key "buffers")
 "bB" '(switch-to-buffer :which-key "Switch to buffer")
 "h" '(dired-up-directory :wk "Move up the directory tree")
 "l" '(dired-find-file :wk "Move into the directory tree")
 )


(leaderkey
  "t" '(:ignore t :wk "Toggle")
  "t l" '(display-line-numbers-mode :wk "Toggle line numbers")
  "t t" '(visual-line-mode :wk "Toggle truncated lines")
  "t n" '(neotree-toggle :wk "Toggle neotree")
  "t v" '(vterm :wk "Open Vterm"))
(leaderkey
  "o" '(:ignore t :wk "Org Mode")
  "o o" '(org-info :wk "Org Mode Manual")
  "o e" '(:ignore t :wk "Export")
  "o e m" '(org-md-export-to-markdown :wk "Export to Markdown")
  "o e p" '(org-babel-tangle :language "python" :wk "Export to Python")
  "o e h" '(org-babel-tangle :language "haskell" :wk "Export to Haskell")
  "o t" '(:ignore t :wk "Toggle")
  "o t b" '(org-babel-tangle :wk "Tangle Org Babel")
  "o t i" '(org-toggle-item:wk "Toggle Item")
  "o a" '(org-agenda :wk "Org Agenda")
  "o c" '(org-capture :wk "Org Capture")
  "o l" '(org-store-link :wk "Store Org Link")
  )
(leaderkey
  "w" '(:ignore t :wk "Windows")
  ;; Window splits
  "w c" '(evil-window-delete :wk "Close window")
  "w n" '(evil-window-new :wk "New window")
  "w s" '(evil-window-split :wk "Horizontal split window")
  "w v" '(evil-window-vsplit :wk "Vertical split window")
  ;; Window motions
  "w h" '(evil-window-left :wk "Window left")
  "w j" '(evil-window-down :wk "Window down")
  "w k" '(evil-window-up :wk "Window up")
  "w l" '(evil-window-right :wk "Window right")
  "w w" '(evil-window-next :wk "Goto next window")
  ;; Move Windows
  "w H" '(buf-move-left :wk "Buffer move left")
  "w J" '(buf-move-down :wk "Buffer move down")
  "w K" '(buf-move-up :wk "Buffer move up")
  "w L" '(buf-move-right :wk "Buffer move right"))

(unless (package-installed-p 'all-the-icons)
  (package-install 'all-the-icons))
(unless (package-installed-p 'all-the-icons-dired)
  (package-install 'all-the-icons-dired))

(require 'all-the-icons)
(require 'all-the-icons-dired)

(add-hook 'dired-mode-hook (lambda () (all-the-icons-dired-mode t)))

(unless (package-installed-p 'auto-complete)
  (package-install 'auto-complete))
(require 'auto-complete)
;;(un)
(ac-config-default)
(global-auto-complete-mode t)
(ac-set-trigger-key "TAB")

;; Adds Auto-complete-mode to org-mode
;;(add-to-list 'ac-modes 'org-mode)

;;(global-set-key (kbd "<tab>") #'company-indent-or-complete-common)
;;(with-eval-after-load 'company
;;  (define-key company-active-map
;;              (kbd "TAB")
;;              ;;#'company-complete-common-or-cycle)
;;              #'company-indent-or-complete-common)
;;  (define-key company-active-map
;;              (kbd "<backtab>")
;;              (lambda ()
;;                (interactive)
;;                (company-complete-common-or-cycle -1))))

(global-auto-revert-mode 1)

(setq backup-directory-alist '((".*" . "~/.local/share/Trash/files")))

(unless (package-installed-p 'beacon)
(package-install 'beacon))
(require 'beacon)
(beacon-mode 1)

(require 'windmove)

;;;###autoload
(defun buf-move-up ()
  "Swap the current buffer and the buffer above the split.
If there is no split, ie now window above the current one, an
error is signaled."
;;  "Switches between the current buffer, and the buffer above the
;;  split, if possible."
  (interactive)
  (let* ((other-win (windmove-find-other-window 'up))
	 (buf-this-buf (window-buffer (selected-window))))
    (if (null other-win)
        (error "No window above this one")
      ;; swap top with this one
      (set-window-buffer (selected-window) (window-buffer other-win))
      ;; move this one to top
      (set-window-buffer other-win buf-this-buf)
      (select-window other-win))))

;;;###autoload
(defun buf-move-down ()
"Swap the current buffer and the buffer under the split.
If there is no split, ie now window under the current one, an
error is signaled."
  (interactive)
  (let* ((other-win (windmove-find-other-window 'down))
	 (buf-this-buf (window-buffer (selected-window))))
    (if (or (null other-win) 
            (string-match "^ \\*Minibuf" (buffer-name (window-buffer other-win))))
        (error "No window under this one")
      ;; swap top with this one
      (set-window-buffer (selected-window) (window-buffer other-win))
      ;; move this one to top
      (set-window-buffer other-win buf-this-buf)
      (select-window other-win))))

;;;###autoload
(defun buf-move-left ()
"Swap the current buffer and the buffer on the left of the split.
If there is no split, ie now window on the left of the current
one, an error is signaled."
  (interactive)
  (let* ((other-win (windmove-find-other-window 'left))
	 (buf-this-buf (window-buffer (selected-window))))
    (if (null other-win)
        (error "No left split")
      ;; swap top with this one
      (set-window-buffer (selected-window) (window-buffer other-win))
      ;; move this one to top
      (set-window-buffer other-win buf-this-buf)
      (select-window other-win))))

;;;###autoload
(defun buf-move-right ()
"Swap the current buffer and the buffer on the right of the split.
If there is no split, ie now window on the right of the current
one, an error is signaled."
  (interactive)
  (let* ((other-win (windmove-find-other-window 'right))
	 (buf-this-buf (window-buffer (selected-window))))
    (if (null other-win)
        (error "No right split")
      ;; swap top with this one
      (set-window-buffer (selected-window) (window-buffer other-win))
      ;; move this one to top
      (set-window-buffer other-win buf-this-buf)
      (select-window other-win))))

;;(unless (package-installed-p 'company)
;;  (package-install 'company))
;;    (setq company-begin-commands '(self-insert-command))
;;    (setq company-idle-delay .1)
;;    (setq company-minimum-prefix-length 2)
;;    (setq company-show-numbers t)
;;    (setq company-tooltip-align-annotations 't)
;;    (setq global-company-mode t)
;;(unless (package-installed-p 'company-box)
;;  (package-install 'company-box))
;;(require 'company)
;;(require 'company-box)
;;
;;(add-hook 'after-init-hook 'global-company-mode)
;;
;;(add-hook 'company-mode 'company-box-mode)

(unless (package-installed-p 'dashboard)
  (package-install 'dashboard))
(require 'dashboard)
(setq initial-buffer-choice 'dashboard-open)
(setq dashboard-set-heading-icons t)
(setq dashboard-set-file-icons t)
(setq dashboard-banner-logo-title "Emacs Is More Than A Text Editor!")
(setq dashboard-startup-banner 'logo) ;; use standard emacs logo as banner
(setq dashboard-startup-banner "~/.config/emacs/shattermacs-1.txt")  ;; use custom image as banner
(setq dashboard-center-content t) ;; set to 't' for centered content
(setq dashboard-items '((recents . 5)
			(agenda . 5 )
			(bookmarks . 3)
			;;(projects . 3) ;; requires Projectile
			(registers . 3)))
;;(dashboard-modify-heading-icons '((recents . "file-text")
;;                                  (bookmarks . "book")))
(dashboard-setup-startup-hook)

(defun my-disable-line-numbers ()
  "Disable line numbers in the dashboard buffer."
  (display-line-numbers-mode -1))

(add-hook 'dashboard-mode-hook #'my-disable-line-numbers)

(unless (package-installed-p 'diminish)
  (package-install 'diminish))
(require 'diminish)
(diminish 'rainbow-mode)
(diminish 'org-mode)
(diminish 'flycheck-mode)
(diminish 'company-mode)
(diminish 'ivy-mode)
(diminish 'counsel-mode)
(diminish 'which-key-mode)

(setq dired-open-extensions '(("gif" . "ristretto")
                              ("jpg" . "ristretto")
                              ("png" . "ristretto")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))

(setq delete-by-moving-to-trash t
      trash-directory "~/.local/share/Trash/files")

(unless (package-installed-p 'elfeed)
(package-install 'elfeed))
(require 'elfeed)
(setq elfeed-search-feed-face ":foreground #ffffff :weight bold"
      elfeed-feeds (quote
                    (("https://www.reddit.com/r/awesomewm.rss"       reddit linux)
                      ("https://www.reddit.com/r/distrotube.rss"     reddit linux)
                      ("https://www.reddit.com/r/linux_gaming.rss"   reddit linux)
                      ("https://www.reddit.com/r/linux.rss"          reddit linux)
                      ("https://www.reddit.com/r/linux4noobs.rss"    reddit linux)
                      ("https://www.reddit.com/r/linuxquestions.rss" reddit linux)
                      ("https://www.reddit.com/r/nixos.rss"          reddit linux)
                      ("https://www.reddit.com/r/commandline.rss"    reddit linux tech)
                      ("https://www.reddit.com/r/emacs.rss"          reddit linux tech)
                      ("https://www.reddit.com/r/foss.rss"           reddit linux tech)
                      ("https://www.reddit.com/r/opensource.rss"     reddit linux tech)
                      ("https://www.reddit.com/r/mobile_linux.rss"   reddit linux tech mobile)
                      ("https://www.reddit.com/r/efreebies.rss"      reddit tech)
                      ("https://www.reddit.com/r/europrivacy.rss"    reddit tech)
                      ("https://www.reddit.com/r/framework.rss"      reddit tech)
                      ("https://www.reddit.com/r/hacking.rss"        reddit tech)
                      ("https://www.reddit.com/r/indiedev.rss"       reddit tech)
                      ("https://www.reddit.com/r/lineageos.rss"      reddit tech mobile)
                      ("https://www.reddit.com/r/microg.rss"         reddit tech mobile)
                      ("https://itsfoss.com/feed/" itsfoss linux news)
                      ("https://www.phoronix.com/rss.php" phoronix linux news)
		        ("https://www.youtube.com/channel/UCO5QSoES5yn2Dw7YixDYT5Q" yt-general) ;; Aperture
		        ("https://www.youtube.com/channel/UCbKdotYtcY9SxoU8CYAXdvg" yt-general) ;; Joseph Hogue
		        ("https://www.youtube.com/channel/UCm9K6rby98W8JigLoZOh6FQ" yt-general) ;; LockPickingLawyer
		        ("https://www.youtube.com/channel/UCl2mFZoRqjw_ELax4Yisf6w" yt-general) ;; Louis Rossmann
		        ("https://www.youtube.com/channel/UC7_YxT-KID8kRbqZo7MyscQ" yt-general) ;; Markiplier
		        ("https://www.youtube.com/channel/UCb31gOY6OD8ES0zP8M0GhAw" yt-general) ;; Max Fosh
		        ("https://www.youtube.com/channel/UCItHdUEqlpfvDlcCeyZwH6w" yt-general) ;; Men of the West
		        ("https://www.youtube.com/channel/UCO3tlaeZ6Z0ZN5frMZI3-uQ" yt-general) ;; Nate O'Brien
		        ("https://www.youtube.com/channel/UC-tLyAaPbRZiYrOJxAGB7dQ" yt-general) ;; Pursuit of Wonder
		        ("https://www.youtube.com/channel/UC4zt4qDcBH5OcSRth5PbZqw" yt-general watches) ;; Ben's Watch Club
		        ("https://www.youtube.com/channel/UCzllztCuniR_83Fwuz70xcg" yt-general watches) ;; JOMW
		        ("https://www.youtube.com/channel/UCwNq5wCP71o59cQ2qyT5fXw" yt-general watches) ;; Just The Watch
		        ("https://www.youtube.com/channel/UCXPXfAAo-yV6Y-0PZecwBLw" yt-general watches) ;; Nico Leonard
		        ("https://www.youtube.com/channel/UCLGp7H4XuzA9TLJ0L4PUx8w" yt-general watches) ;; Teddy Baldassarre
		        ("https://www.youtube.com/channel/UCED3DPNk9DHQn2MRogOd-CA" yt-general watches) ;; The Mad Watch Collector
		        ("https://www.youtube.com/channel/UCLaoR2K7Dsa_KUK-DVpdZZA" yt-general watches) ;; Watchfinder & Co.
		        ("https://www.youtube.com/channel/UCS0N5baNlQWJCUrhCEo8WlA" yt-tech) ;; Ben Eater
		        ("https://www.youtube.com/channel/UCg6gPGh8HU2U01vaFCAsvmQ" yt-tech) ;; Chris Titus Tech
		        ("https://www.youtube.com/channel/UC0e3QhIYukixgh5VVpKHH9Q" yt-tech) ;; Code Bullet
		        ("https://www.youtube.com/channel/UCaSCt8s_4nfkRglWCvNSDrg" yt-tech) ;; CodeAesthetic
		        ("https://www.youtube.com/channel/UC9-y-6csu5WGm29I7JiwpnA" yt-tech) ;; Computerphile
		        ("https://www.youtube.com/channel/UCVeW9qkBjo3zosnqUbG7CFw" yt-tech) ;; John Hammond
		        ("https://www.youtube.com/channel/UCP2bshADPrVMoNrdJvZEQzw" yt-tech) ;; Keep it Techie
		        ("https://www.youtube.com/channel/UC9x0AN7BWHpCDHSm9NiJFJQ" yt-tech) ;; NetworkChuck
		        ("https://www.youtube.com/channel/UCjSEJkpGbcZhvo0lr-44X_w" yt-tech) ;; TechHut
		        ("https://www.youtube.com/channel/UCs6KfncB4OV6Vug4o_bzijg" yt-tech) ;; Techlore
		        ("https://www.youtube.com/channel/UCtYg149E_wUGVmjGz-TgyNA" yt-tech) ;; Titus Tech Talk
		        ("https://www.youtube.com/channel/UCTd7Cqj-oAEONRcG53Pg0ew" yt-tech) ;; Nicholas Bernstein
		        ("https://www.youtube.com/channel/UCNvl_86ygZXRuXjxbONI5jA" yt-tech linux) ;; 10leej
		        ("https://www.youtube.com/channel/UCld68syR8Wi-GY_n4CaoJGA" yt-tech linux) ;; Brodie Robertson
		        ("https://www.youtube.com/channel/UCVls1GmFKf6WlTraIb_IaJg" yt-tech linux) ;; Distrotube
		        ("https://www.youtube.com/channel/UC05XpvbHZUQOfA6xk4dlmcw" yt-tech linux) ;; DJWare
		        ("https://www.youtube.com/channel/UCJdmdUp5BrsWsYVQUylCMLg" yt-tech linux) ;; Erik Dubois
		        ("https://www.youtube.com/channel/UC3jSNmKWYA04R47fDcc1ImA" yt-tech linux) ;; InfinitelyGalactic
		        ("https://www.youtube.com/channel/UCxQKHvKbmSzGMvUrVtJYnUA" yt-tech linux) ;; LearnLinux TV
		        ("https://www.youtube.com/channel/UCylGUf9BvQooEFjgdNudoQg" yt-tech linux) ;; The Linux Cast
		        ("https://www.youtube.com/channel/UC5UAwBUum7CPN5buc-_N1Fw" yt-tech linux) ;; The Linux Experiment
		        ("https://www.youtube.com/channel/UC7pdnrWVj8eDfCI0bRe_0kQ" yt-tech linux) ;; Linux Training Academy
		        ("https://www.youtube.com/channel/UCONH73CdRXUjlh3-DdLGCPw" yt-tech linux) ;; Nicco Loves Linux
		        ("https://www.youtube.com/channel/UCBq5p-xOla8xhnrbhu8AIAg" yt-tech linux) ;; Tech Over Tea
		        ("https://www.youtube.com/channel/UCsnGwSIHyoYN0kiINAGUKxg" yt-tech linux) ;; Wolfgang's Channel
		        ("https://www.youtube.com/channel/UCmw-QGOHbHA5cDAvwwqUTKQ" yt-tech linux) ;; Zaney (Tyler)
                      )))

(unless (package-installed-p 'elfeed-goodies)
(package-install 'elfeed-goodies))
(require 'elfeed-goodies)
(elfeed-goodies/setup)
(setq elfeed-goodies/entry-pane-size 0.5)

(unless (package-installed-p 'emms)
    (package-install 'emms))
  (require 'emms)
  (emms-all)
  (emms-default-players)
  (emms-mode-line 0) ;; Some tracks use special characters which can make the modeline larger and break things
  (emms-playing-time 1)
  (setq emms-source-file-default-directory "~/Music/Music/Playlists/M3U-PC/"
	emms-playlist-buffer-name "*Music*"
	emms-info-asynchronously t
	emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)

;; Automatically shuffle a playlist before playing it.
(add-hook 'emms-playlist-source-inserted-hook 'emms-shuffle)

;;

;;(setq browse-url-browser-function 'eww-browse-url)
(setq browse-url-browser-function 'browse-url-xdg-open)

;;(unless (package-installed-p 'exwm)
;;  (package-install 'exwm))
;;(require 'exwm)
;;(require 'exwm-config)
;;(exwm-config-default)
;;(require 'exwm-randr)
;;(setq exwm-randr-workspace-output-plist '(0 "eDP-1"))
;;(add-hook 'exwm-randr-screen-change-hook 
;;	  (lambda ()
;;	    (start-process-shell-command
;;	     "xrandr" nil "xrandr -s 1920x1080 --pos 0x0 --rotate normal")))
;;(exwm-randr-enable)
;;(require 'exwm-systemtray)
;;(exwm-systemtray-enable)

(unless (package-installed-p 'flycheck)
  (package-install 'flycheck))
(global-flycheck-mode)

(set-face-attribute 'default nil
  :font "Hack"
  :height 145
  :weight 'medium)
(set-face-attribute 'variable-pitch nil
  :font "Hack"
  :height 145
  :weight 'medium)
(set-face-attribute 'fixed-pitch nil
  :font "Hack"
  :height 145
  :weight 'medium)
;; Makes commented text and keywords italics.
;; This is working in emacsclient but not emacs.
;; Your font must have an italic face available.
(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
  :slant 'italic)

;; This sets the default font on all graphical frames created after restarting Emacs.
;; Does the same thing as 'set-face-attribute default' above, but emacsclient fonts
;; are not right unless I also add this method of setting the default font.
(add-to-list 'default-frame-alist '(font . "Hack-14"))

;; Uncomment the following line if line spacing needs adjusting.
(setq-default line-spacing 0.12)

(defun reset-text-scale ()
  "Reset the text scale to default (normal) size."
  (interactive)
  (text-scale-set 0))

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)
(global-set-key (kbd "C-0") 'reset-text-scale)

(unless (package-installed-p 'hl-todo)
(package-install 'hl-todo))
(require 'hl-todo)
;;(use-package 'hl-todo) 
(add-hook 'org-mode-hook (lambda () (hl-todo-mode 1)))
(add-hook 'prog-mode-hook (lambda () (hl-todo-mode 1)))
(setq hl-todo-highlight-punctuation ":"
      hl-todo-keyword-faces
      `(("TODO"       warning bold)
        ("FIXME"      error bold)
        ("HACK"       font-lock-constant-face bold)
        ("REVIEW"     font-lock-keyword-face bold)
        ("NOTE"       success bold)
        ("DEPRECATED" font-lock-doc-face bold)))

(unless (package-installed-p 'ivy)
  (package-install 'ivy))
(require 'ivy)
(unless (package-installed-p 'counsel)
  (package-install 'counsel))
(require 'counsel)
(unless (package-installed-p 'all-the-icons-ivy-rich)
  (package-install 'all-the-icons-ivy-rich))
(require 'all-the-icons-ivy-rich)
(unless (package-installed-p 'ivy-rich)
  (package-install 'ivy-rich))
(require 'ivy-rich)

(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")
(setq enable-recursive-minibuffers t)
(ivy-mode)
(counsel-mode)
(all-the-icons-ivy-rich-mode 1)
(ivy-rich-mode 1)
(setq ivy-initial-inputs-alist nil)
(setq ivy-virtual-abbreviate 'full)
(setq ivy-rich-switch-buffer-align-virtual-buffer t)
(setq ivy-rich-path-style 'abbrev)
(setq ivy-set-display-transformer 'ivy-switch-buffer)
(setq ivy-set-display-transformer 'ivy-rich-switch-buffer-transformer)

(unless (package-installed-p 'haskell-mode)
  (package-install 'haskell-mode))
(require 'haskell-mode)

(unless (package-installed-p 'lua-mode)
  (package-install 'lua-mode))
(require 'lua-mode)

(unless (package-installed-p 'lsp-java)
  (package-install 'lsp-java))
(require 'lsp-java)
(add-hook 'java-mode-hook #'lsp)

(unless (package-installed-p 'markdown-mode)
  (package-install 'markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-hook 'markdown-mode-hook 'turn-on-auto-fill)
(setq markdown-indent-on-enter nil) ; Disable indentation on pressing Enter
()
(unless (package-installed-p 'markdown-preview-mode)
  (package-install 'markdown-preview-mode))

(custom-set-faces
 '(markdown-header-face-1 ((t (:inherit markdown-header-face :height 1.7))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :height 1.6))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.5))))
 '(markdown-header-face-4 ((t (:inherit markdown-header-face :height 1.4))))
 '(markdown-header-face-5 ((t (:inherit markdown-header-face :height 1.3))))
 '(markdown-header-face-6 ((t (:inherit markdown-header-face :height 1.2))))
 '(markdown-header-face-7 ((t (:inherit markdown-header-face :height 1.1)))))

(defvar nb/current-line '(0 . 0)
   "(start . end) of current line in current buffer")
 (make-variable-buffer-local 'nb/current-line)

 (defun nb/unhide-current-line (limit)
   "Font-lock function"
   (let ((start (max (point) (car nb/current-line)))
         (end (min limit (cdr nb/current-line))))
     (when (< start end)
       (remove-text-properties start end
                       '(invisible t display "" composition ""))
       (goto-char limit)
       t)))

 (defun nb/refontify-on-linemove ()
   "Post-command-hook"
   (let* ((start (line-beginning-position))
          (end (line-beginning-position 2))
          (needs-update (not (equal start (car nb/current-line)))))
     (setq nb/current-line (cons start end))
     (when needs-update
       (font-lock-fontify-block 3))))

 (defun nb/markdown-unhighlight ()
   "Enable markdown concealling"
   (interactive)
   (markdown-toggle-markup-hiding 'toggle)
   (font-lock-add-keywords nil '((nb/unhide-current-line)) t)
   (add-hook 'post-command-hook #'nb/refontify-on-linemove nil t))

;; Autostarting this
(add-hook 'markdown-mode-hook #'nb/markdown-unhighlight)

(global-set-key [escape] 'keyboard-escape-quit)

;;(unless (package-installed-p 'doom-modeline)
;;  (package-install 'doom-modeline))
;;(require 'doom-modeline)
;;(doom-modeline-mode 1)
;;(setq doom-modeline-height 25      ;; sets modeline height
;;      doom-modeline-bar-width 5    ;; sets right bar width
;;      doom-modeline-persp-name t   ;; adds perspective name to modeline
;;      doom-modeline-persp-icon t)

(unless (package-installed-p 'telephone-line)
  (package-install 'telephone-line))

(require 'telephone-line)


(require 'telephone-line-separators)
(require 'telephone-line-segments)

(setq telephone-line-faces
      '(;;(evil . telephone-evil)
        (evil . telephone-line-modal-face)
        ;;(evil . (telephone-evil . telephone-evil-normal))
        (accent . (telephone-line-accent-active . telephone-line-accent-inactive))
        (nil . (mode-line . mode-line-inactive))))

(telephone-line-defsegment* telephone-line-custom-buffer-segment ()
  `(""
    ,(telephone-line-raw "%b" 'face)))

(setq telephone-line-lhs
      '((evil   . (telephone-line-evil-tag-segment))
        (accent . (telephone-line-vc-segment
                   telephone-line-erc-modified-channels-segment
                   telephone-line-process-segment))
        (nil    . (;;telephone-line-minor-mode-segment
                   telephone-line-custom-buffer-segment))))
(setq telephone-line-rhs
      '((nil    . (telephone-line-misc-info-segment))
        (accent . (telephone-line-major-mode-segment))
        (evil   . (telephone-line-airline-position-segment))))

(setq-default mode-line-format '("%e" (:eval (telephone-line-mode-line-format))))

(telephone-line-mode 1)

(unless (package-installed-p 'neotree)
  (package-install 'neotree))
(require 'neotree)

  (setq neo-smart-open t
        neo-show-hidden-files t
        neo-window-width 30
        neo-window-fixed-size nil
        inhibit-compacting-font-caches t
        projectile-switch-project-action 'neotree-projectile-action) 
        ;; truncate long file names in neotree
        (add-hook 'neo-after-create-hook
           #'(lambda (_)
               (with-current-buffer (get-buffer neo-buffer-name)
                 (setq truncate-lines t)
                 (setq word-wrap nil)
                 (make-local-variable 'auto-hscroll-mode)
                 (setq auto-hscroll-mode nil))))

(setq user-full-name "Aleks (theshatterstone)")

(defun my/org-insert-default-content ()
  "Insert default content for new Org mode files."
  (when (and (buffer-file-name)
             (string= (file-name-extension (buffer-file-name)) "org")
             (= (point-min) (point-max)))
    (insert "#+TITLE: \n"
            "#+AUTHOR: " user-full-name "\n\n"
            )
    (goto-char (point-min))
    ;;(forward-line 1)
    (end-of-line)
))

(add-hook 'find-file-hook 'my/org-insert-default-content)

(unless (package-installed-p 'org-auto-tangle)
  (package-install 'org-auto-tangle))
(require 'org-auto-tangle)
(add-hook 'org-mode-hook 'org-auto-tangle-mode)

(unless (package-installed-p 'toc-org)
  (package-install 'toc-org))
(require 'toc-org)
;;(use-package 'toc-org)
(toc-org-enable)
(add-hook 'org-mode-hook 'toc-org-enable)

(add-hook 'org-mode-hook 'org-indent-mode)
(unless (package-installed-p 'org-bullets)
  (package-install 'org-bullets))
(require 'org-bullets)

;;(use-package 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(electric-indent-mode -1)
(setq org-edit-src-content-indentation 0)

;;(require 'color)
;;(set-face-attribute 'org-block nil :background
;;                    (color-darken-name
;;                     (face-attribute 'default :background) 20))

(eval-after-load 'org-indent '(diminish 'org-indent-mode))

(custom-set-faces
 '(org-level-1 ((t (:inherit outline-1 :height 1.5))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.4))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.3))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.3))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.2))))
 '(org-level-6 ((t (:inherit outline-5 :height 1.2))))
 '(org-level-7 ((t (:inherit outline-5 :height 1.1))))
 '(org-level-8 ((t (:inherit outline-5 :height 1.1)))))

(with-eval-after-load 'org
  (setq org-agenda-files '("~/notes/TODO.org"
                           "~/org/*.org")))

(require 'org-tempo)

(unless (package-installed-p 'rainbow-delimiters)
  (package-install 'rainbow-delimiters))
(require 'rainbow-delimiters)
(add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode)
(add-hook 'clojure-mode-hook #'rainbow-delimiters-mode)

(unless (package-installed-p 'rainbow-mode)
  (package-install 'rainbow-mode))
(require 'rainbow-mode)
(add-hook 'org-mode-hook (lambda () (rainbow-mode 1)))
(add-hook 'prog-mode-hook 'rainbow-mode)

(delete-selection-mode 1)    ;; You can select text and delete it by typing.
(electric-indent-mode -1)    ;; Turn off the weird indenting that Emacs does by default.
(electric-pair-mode 1)       ;; Turns on automatic parens pairing
;; The following prevents <> from auto-pairing when electric-pair-mode is on.
;; Otherwise, org-tempo is broken when you try to <s TAB...
(add-hook 'org-mode-hook (lambda ()
           (setq-local electric-pair-inhibit-predicate
                   `(lambda (c)
                  (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))
(global-auto-revert-mode t)  ;; Automatically show changes if the file has changed
(global-display-line-numbers-mode 1) ;; Display line numbers
(setq display-line-numbers-type 'relative) ;; Display relative line numbers (better for evil-mode)
(global-visual-line-mode t)  ;; Enable truncated lines
(menu-bar-mode -1)           ;; Disable the menu bar 
(scroll-bar-mode -1)         ;; Disable the scroll bar
(tool-bar-mode -1)           ;; Disable the tool bar
(setq org-edit-src-content-indentation 0) ;; Set src block automatic indent to 0 instead of 2.

(unless (package-installed-p 'gnu-elpa-keyring-update)
  (package-install 'gnu-elpa-keyring-update))
(require 'gnu-elpa-keyring-update)

(setq server-client-instructions nil)

(defun display-startup-echo-area-message ()
  (message ""))

(setq warning-minimum-level :emergency)

(evil-set-undo-system 'undo-redo)

(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))
        (:eval (if (buffer-modified-p)
                   ""))
        " - Shattermacs")
      )

(defalias 'yes-or-no-p 'y-or-n-p) ;; Until Emacs 28
(setopt use-short-answers t)   ;; Since Emacs 29, `yes-or-no-p' will use `y-or-n-p'

(unless (package-installed-p 'sudo-edit)
  (package-install 'sudo-edit))
(require 'sudo-edit)
(leaderkey
  "fu" '(sudo-edit-find-file :wk "Sudo find file")
  "fU" '(sudo-edit :wk "Sudo edit file"))

(add-to-list 'custom-theme-load-path "~/.config/emacs/themes/")
;;(load-theme 'shattermacs-dark t)
(load-theme 'dark-green t)
;;(if (display-graphic-p)
;;  (load-theme 'shattermacs-dark t)
;;(load-theme 'tty-dark))

(unless (package-installed-p 'tldr)
(package-install 'tldr))
(require 'tldr)

(unless (package-installed-p 'which-key)
  (package-install 'which-key))
(require 'which-key)
  (which-key-mode 1)
  (setq which-key-side-window-location 'bottom
    which-key-sort-order #'which-key-key-order-alpha
    which-key-sort-uppercase-first nil
    which-key-add-column-padding 1
    which-key-max-display-columns nil
    which-key-min-display-lines 6
    which-key-side-window-slot -10
    which-key-side-window-max-height 0.25
    which-key-idle-delay 0.8
    which-key-max-description-length 25
    which-key-allow-imprecise-window-fit nil
    which-key-separator " → " )
