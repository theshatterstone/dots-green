-- Basic QoL Features
vim.cmd("set number")
vim.cmd("set noshowmode")
vim.cmd("set expandtab")
vim.cmd("set tabstop=4")
vim.cmd("set softtabstop=2")
vim.cmd("set shiftwidth=2")
vim.cmd("set clipboard +=unnamedplus")
vim.cmd("nnoremap <esc><esc> :noh<return><esc>")
vim.g.mapleader = " "


