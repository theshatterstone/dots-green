zstyle ':completion:*' menu select
zstyle ':completion:*' list-prompt   ''
zstyle ':completion:*' select-prompt ''
autoload -Uz compinit
compinit
#zstyle ':completion:*' menu select

# Replace Emacs-style keybinds with VI-style keybinds
# set -o vi

export PATH="$HOME/.local/bin:$PATH"
export PATH="$PATH:$HOME/.config/emacs/bin"
export PATH="/home/shatterstone/.local/share/solana/install/active_release/bin:$PATH"

export EDITOR="vim"
export VISUAL="nvim"

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory

# Turn off all beeps
unsetopt BEEP
# Turn off autocomplete beeps
# unsetopt LIST_BEEP

alias apt="sudo apt"
alias dnf="sudo dnf5"
alias zypper="sudo zypper"
alias pacman="paru"
alias neofetch="neofetch --color_blocks off"
alias ls="ls -lah"
alias update="sudo pacman -Syyu && flatpak update"
alias vim="nvim"

alias nix-conf="nvim $HOME/.config/nixos/configuration.nix"
alias rebuild-switch="sudo nixos-rebuild switch"
alias nixos-update="sudo nixos-rebuild switch --upgrade"

alias fish-conf="vim $HOME/.config/fish/config.fish"
alias hypr-conf="vim $HOME/.config/hypr/hyprland.conf"
alias river-conf="vim $HOME/.config/river/init"
alias awesome-conf="vim $HOME/.config/awesome/rc.lua"
alias qtile-conf="vim $HOME/.config/qtile/config.py"
alias sway-conf="vim $HOME/.config/sway/config"

# alias clear="clear && neofetch"
ZSH_AUTOSUGGEST_STRATEGY=(history completion)

typeset -A ZSH_HIGHLIGHT_STYLES

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE=fg=#545454

# ZSH_HIGHLIGHT_STYLES[default]=fg=cyan
# ZSH_HIGHLIGHT_STYLES[unknown-token]=fg=red,bright
# ZSH_HIGHLIGHT_STYLES[reserved-word]=fg=blue
# ZSH_HIGHLIGHT_STYLES[alias]=fg=blue
# ZSH_HIGHLIGHT_STYLES[builtin]=fg=blue
# ZSH_HIGHLIGHT_STYLES[function]=fg=cyan
# ZSH_HIGHLIGHT_STYLES[command]=fg=blue
# ZSH_HIGHLIGHT_STYLES[precommand]=fg=blue
# ZSH_HIGHLIGHT_STYLES[commandseparator]=fg=cyan
# ZSH_HIGHLIGHT_STYLES[hashed-command]=fg=blue
# ZSH_HIGHLIGHT_STYLES[path]=fg=cyan,underline
# ZSH_HIGHLIGHT_STYLES[globbing]=fg=none
# ZSH_HIGHLIGHT_STYLES[history-expansion]=fg=cyan
# ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=fg=cyan
# ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=fg=cyan
# ZSH_HIGHLIGHT_STYLES[back-quoted-argument]=fg=yellow
# ZSH_HIGHLIGHT_STYLES[single-quoted-argument]=fg=yellow
# ZSH_HIGHLIGHT_STYLES[double-quoted-argument]=fg=yellow
# ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]=fg=yellow
# ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]=fg=yellow
# ZSH_HIGHLIGHT_STYLES[assign]=fg=cyan

ZSH_HIGHLIGHT_STYLES[default]=fg=2
ZSH_HIGHLIGHT_STYLES[unknown-token]=fg=red,bright
ZSH_HIGHLIGHT_STYLES[reserved-word]=fg=10
ZSH_HIGHLIGHT_STYLES[alias]=fg=10
ZSH_HIGHLIGHT_STYLES[builtin]=fg=10
ZSH_HIGHLIGHT_STYLES[function]=fg=2
ZSH_HIGHLIGHT_STYLES[command]=fg=10
ZSH_HIGHLIGHT_STYLES[precommand]=fg=10
ZSH_HIGHLIGHT_STYLES[commandseparator]=fg=2
ZSH_HIGHLIGHT_STYLES[hashed-command]=fg=10
ZSH_HIGHLIGHT_STYLES[path]=fg=2,underline
ZSH_HIGHLIGHT_STYLES[globbing]=fg=none
ZSH_HIGHLIGHT_STYLES[history-expansion]=fg=2
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=fg=2
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=fg=2
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]=fg=yellow
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]=fg=yellow
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]=fg=yellow
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]=fg=yellow
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]=fg=yellow
ZSH_HIGHLIGHT_STYLES[assign]=fg=2


#neofetch --color_blocks off
#colorscript -r
#$HOME/Documents/fetch/fetch.sh
#pcfetch
#pfetch
echo " "
neofetch

source $HOME/.config/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source $HOME/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Starship Prompt
eval "$(starship init zsh)"

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

[ -f "/home/shatterstone/.ghcup/env" ] && source "/home/shatterstone/.ghcup/env" # ghcup-env

