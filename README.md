# Green Theme Dotfiles

## Theming

Use Adapta-Nokto as GTK Theme And Papirus with the Teal Icons
that can be installed from here:

```
https://github.com/PapirusDevelopmentTeam/papirus-folders
```

and then running:

```
papirus-folders -C teal --theme Papirus-Dark
```

Firefox theme: Aquamarine Night by Manuel Garcia

Firefox wallpaper: [[firefox-bg.png](firefox-bg.png)]

Desktop wallpaper: [[wall.png](wall.png)]
